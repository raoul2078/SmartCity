package fr.eklouadjakly.smartcity.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.EventsAdapter;
import fr.eklouadjakly.smartcity.model.Event;

import static android.Manifest.permission.READ_CALENDAR;
import static android.Manifest.permission.WRITE_CALENDAR;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventsFragment extends Fragment {

    private static final String TAG = "EventsFragment";
    private static final int REQUEST_ACCESS_CALENDAR = 164;
    Cursor cursor;

    private OnFragmentInteractionListener mListener;

    NestedScrollView eventsView;

    FloatingActionButton fab;

    private List<Event> events = new ArrayList<>();
    private RecyclerView recyclerView;

    public EventsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventsFragment.
     */
    public static EventsFragment newInstance() {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mayRequestPermission();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        eventsView = getActivity().findViewById(R.id.eventsView);
        recyclerView = getActivity().findViewById(R.id.eventsrv);
        fab = getActivity().findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar beginTime = Calendar.getInstance();
                Calendar endTime = Calendar.getInstance();
                endTime.add(Calendar.DATE, 1);
                Intent intent = new Intent(Intent.ACTION_INSERT);
                intent.setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                ;
                startActivity(intent);
            }
        });

        if (ActivityCompat.checkSelfPermission(getActivity(), READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            mayRequestPermission();
        } else {
            getEvents();
        }
    }

    private boolean mayRequestPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                && getActivity().checkSelfPermission(WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CALENDAR) || shouldShowRequestPermissionRationale(WRITE_CALENDAR)) {
            Snackbar.make(getActivity().findViewById(R.id.news_viewpager), R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CALENDAR, WRITE_CALENDAR}, REQUEST_ACCESS_CALENDAR);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CALENDAR, WRITE_CALENDAR}, REQUEST_ACCESS_CALENDAR);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_CALENDAR) {
            if (grantResults.length > 0) {
                boolean permission = (grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED);
                if (permission) {
                    getEvents();
                }
            }
        }

    }

    private void getEvents() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            mayRequestPermission();
        } else {
            String[] projection = new String[]{CalendarContract.Events._ID, CalendarContract.Events.TITLE, CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND, CalendarContract.Events.ALL_DAY, CalendarContract.Events.EVENT_LOCATION};

            Calendar startTime = Calendar.getInstance();

            startTime.set(Calendar.HOUR_OF_DAY, 0);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.SECOND, 0);

            String selection = "(( " + CalendarContract.Events.DTSTART + " >= " + startTime.getTimeInMillis() + " )  AND ( deleted != 1 ))";


            cursor = getActivity().getContentResolver()
                    .query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null);

            while (cursor.moveToNext()) {
                if (cursor != null) {
                    int id = cursor.getColumnIndex(CalendarContract.Events._ID);
                    int titleId = cursor.getColumnIndex(CalendarContract.Events.TITLE);
                    int descriptionId = cursor.getColumnIndex(CalendarContract.Events.DESCRIPTION);
                    int locationId = cursor.getColumnIndex(CalendarContract.Events.EVENT_LOCATION);
                    int beginId = cursor.getColumnIndex(CalendarContract.Events.DTSTART);
                    int endId = cursor.getColumnIndex(CalendarContract.Events.DTEND);
                    long idValue = cursor.getLong(id);
                    String titleValue = cursor.getString(titleId);
                    String descriptionValue = cursor.getString(descriptionId);
                    String locationValue = cursor.getString(locationId);
                    DateFormat df = DateFormat.getDateTimeInstance();

                    String begin;
                    String end;

                    Log.w(TAG, "getEvents: begin " + cursor.getLong(beginId) + " end " + cursor.getLong(endId));

                    if (cursor.getLong(beginId) > 0 && cursor.getLong(endId) > 0) {
                        begin = df.format(cursor.getLong(beginId));
                        end = df.format(cursor.getLong(endId));
                        events.add(new Event(
                                idValue,
                                titleValue,
                                descriptionValue,
                                locationValue,
                                begin,
                                end
                        ));
                    }
//                    Toast.makeText(getActivity(), titleValue + ", " + descriptionValue + ", " + locationValue, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Event is not present", Toast.LENGTH_LONG).show();
                }
            }
            Log.w(TAG, "getEvents: " + events.size());
            cursor.close();
            setEventsAdapater(events);
        }
    }

    private void setEventsAdapater(List<Event> events) {
        EventsAdapter myAdapter = new EventsAdapter(getContext(), events);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(myAdapter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
