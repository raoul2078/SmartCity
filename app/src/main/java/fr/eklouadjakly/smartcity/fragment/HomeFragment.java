package fr.eklouadjakly.smartcity.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.activity.MainActivity;
import fr.eklouadjakly.smartcity.database.DatabaseHandler;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = "HomeFragment";
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PROFILE = "profile";

    // TODO: Rename and change types of parameters
    private String urlProfile;
    private Bitmap bitmap;

    private OnFragmentInteractionListener mListener;

    private ImageView imageView, imageViewBis;
    private CardView newsCard, shopsCard, socialsCard, advertsCard;
    private DatabaseHandler handler;
    TokenManager tokenManager;
    GoogleApiClient gac;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance(String param1) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PROFILE, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PROFILE, null);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            urlProfile = getArguments().getString(ARG_PROFILE);
        }
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = getActivity().findViewById(R.id.homeProfile);
        imageViewBis = getActivity().findViewById(R.id.homeProfileBis);
        getActivity().setTitle(R.string.menu_home);

        if (urlProfile != null) {
            GlideApp.with(this)
                    .load(urlProfile)
                    .signature(new ObjectKey(tokenManager.getUUID()))
                    .transition(withCrossFade())
                    .transform(new CircleTransform(getActivity()))
                    // .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imageView);
        } else {

            GlideApp.with(this)
                    .load(urlProfile)
                    .signature(new ObjectKey(tokenManager.getUUID()))
                    .transition(withCrossFade())
                    .transform(new CircleTransform(getActivity()))
                    // .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imageView);
            Log.w(TAG, "Find place picture");

          /*  gac = new GoogleApiClient
                    .Builder(getActivity())
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(getActivity(), this)
                    .build();
            gac.connect();
            final String placeId = "ChIJ4zGFAZpYwokRGUGph3Mf37k";
            Places.GeoDataApi.getPlacePhotos(gac, placeId)
                    .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {
                        @Override
                        public void onResult(PlacePhotoMetadataResult photos) {
                            if (!photos.getStatus().isSuccess()) {
                                Log.w(TAG, "onResult: photo no success" + photos.getStatus() );
                                return;
                            }
                            Log.w(TAG, "onResult: photo  success" + photos.getStatus() );

                            PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                            if (photoMetadataBuffer.getCount() > 0) {
                                // Display the first bitmap in an ImageView in the size of the view
                                System.out.println("photos detail " + photos.toString());
                                photoMetadataBuffer.get(0)
                                        .getPhoto(gac)
                                        .setResultCallback(mDisplayPhotoResultCallback);
                                CharSequence attribution = photoMetadataBuffer.get(0).getAttributions();
                            }
                            photoMetadataBuffer.release();
                        }
                    });*/
        }

        newsCard = getActivity().findViewById(R.id.newscard);
        shopsCard = getActivity().findViewById(R.id.shopscard);
        socialsCard = getActivity().findViewById(R.id.socialscard);
        advertsCard = getActivity().findViewById(R.id.advertscard);

        newsCard.setOnClickListener(this);
        shopsCard.setOnClickListener(this);
        socialsCard.setOnClickListener(this);
        advertsCard.setOnClickListener(this);
    }

    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
            = new ResultCallback<PlacePhotoResult>() {
        @Override
        public void onResult(PlacePhotoResult placePhotoResult) {
            if (!placePhotoResult.getStatus().isSuccess()) {
                Log.w(TAG, "photoresult no success "+ placePhotoResult.getStatus() );
                return;
            }
            Bitmap bitmap = placePhotoResult.getBitmap();
            Log.w(TAG, "bitmap size "+bitmap.getWidth() + " " + bitmap.getHeight() );
            imageViewBis.setImageBitmap(bitmap);

        }
    };

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == newsCard.getId() || view.getId() == shopsCard.getId() || view.getId() == socialsCard.getId() || view.getId() == advertsCard.getId()) {
            ((MainActivity) getActivity()).selectItemDrawerByCard(view.getId());
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
       /* if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Please allow ACCESS_COARSE_LOCATION persmission.",
                    Toast.LENGTH_LONG).show();
            return;
        }*/
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "onConnectionSuspended: " + i);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*GlideApp.with(this)
                .load(urlProfile)
                .signature(new ObjectKey(tokenManager.getUUID()))
                .transition(withCrossFade())
                .transform(new CircleTransform(getActivity()))
                // .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);*/
    }


    @Override
    public void onStop() {
        super.onStop();
        if (gac != null && gac.isConnected()) {
            gac.stopAutoManage(getActivity());
            gac.disconnect();
        }
    }
}
