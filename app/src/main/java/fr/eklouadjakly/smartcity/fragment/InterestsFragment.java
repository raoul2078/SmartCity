package fr.eklouadjakly.smartcity.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Interest;
import fr.eklouadjakly.smartcity.model.InterestRequest;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InterestsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InterestsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InterestsFragment extends PreferenceFragmentCompat {

    private static final String TAG = "InterestsFragment";
    ApiService service;
    Call<User> call;
    TokenManager tokenManager;
    User user;

    private OnFragmentInteractionListener mListener;

    public InterestsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment InterestsFragment.
     */
    public static InterestsFragment newInstance() {
        InterestsFragment fragment = new InterestsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service  = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        //Charger les préférences
        addPreferencesFromResource(R.xml.app_interests_prefs);
    }

  /*  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_interests, container, false);
    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        user.setInterests(getInterests());
        tokenManager.saveUser(user);
        call = service.updateUserInterests(user.getId(), new InterestRequest(getInterestsAsString()));
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.w(TAG, "onResponse interestsfragment ok ");

                } else {
                    ApiError apiError = Utils.convertErrors(response.errorBody());
                    Log.w(TAG, "onResponse: no success " + apiError.getMessage());

                }
                if (call != null) {
                    call.cancel();
                    call = null;
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.w(TAG, "onFailure interestsfragment "+t.getMessage() );
                if (call != null) {
                    call.cancel();
                    call = null;
                }
            }
        });
    }

    private List<Interest> getInterests() {
        List<Interest> interests = new ArrayList<>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        String[] interestsItems = getResources().getStringArray(R.array.interests_items);
        for (String item : interestsItems) {
            boolean isChecked = preferences.getBoolean(item, false);
            if (isChecked) {
                interests.add(new Interest(item));
            }
        }
        return interests;
    }

    private List<String> getInterestsAsString() {
        List<String> interests = new ArrayList<>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        String[] interestsItems = getResources().getStringArray(R.array.interests_items);
        for (String item : interestsItems) {
            boolean isChecked = preferences.getBoolean(item, false);
            if (isChecked) {
                interests.add(item);
            }
        }
        return interests;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
