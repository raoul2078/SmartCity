package fr.eklouadjakly.smartcity.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.util.Calendar;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.receiver.AlarmReceiver;
import fr.eklouadjakly.smartcity.utils.TokenManager;

import static android.content.Context.ALARM_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AlarmFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AlarmFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlarmFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "AlarmFragment";

    TokenManager tokenManager;

    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker alarmTimePicker;
    private TextView alarmTextView;
    Calendar calendar = null;
    private OnFragmentInteractionListener mListener;

    public AlarmFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AlarmFragment.
     */
    public static AlarmFragment newInstance(String param1, String param2) {
        AlarmFragment fragment = new AlarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alarm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        alarmTimePicker = (TimePicker) getActivity().findViewById(R.id.alarmTimePicker);
        alarmTimePicker.setIs24HourView(DateFormat.is24HourFormat(getActivity()));
        alarmTextView = (TextView) getActivity().findViewById(R.id.alarmText);
        ToggleButton toggleButton = getActivity().findViewById(R.id.alarmToggle);
        toggleButton.setOnClickListener(this);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));
        toggleButton.setChecked(tokenManager.isAlarmSet());
        setAlarmText("Heure: " + tokenManager.getAlarm());
    }


    public void onToggleClicked(View view) {
        alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        Intent myIntent = new Intent(getContext(), AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(getActivity().getBaseContext(), 0, myIntent, 0);

        if (((ToggleButton) view).isChecked()) {
            Log.d(TAG, "Alarm On");
            calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());

            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);

            String hour = alarmTimePicker.getCurrentHour() + ":" + alarmTimePicker.getCurrentMinute();
            tokenManager.alarmOn();
            tokenManager.setAlarm(hour);
            setAlarmText(hour);
        } else {
            alarmManager.cancel(pendingIntent);
            tokenManager.alarmOff();
            tokenManager.setAlarm("");
            setAlarmText("");
            Log.d(TAG, "Alarm Off");
        }
    }

    public void setAlarmText(String alarmText) {
        alarmTextView.setText(alarmText);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        onToggleClicked(v);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
