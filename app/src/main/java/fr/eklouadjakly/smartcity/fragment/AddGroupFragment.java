package fr.eklouadjakly.smartcity.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.FilePath;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddGroupFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddGroupFragment extends Fragment {

    private static final String TAG = "EditProfileActivity";
    private static final int PICK_FILE_REQUEST = 1;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 2;

    private String selectedFilePath;

    LinearLayout addGroupll;
    ImageView groupImageView;
    TextInputLayout tilName;
    TextInputLayout tilDescription;
    CheckBox checkIsPublic;

    ApiService service;
    TokenManager tokenManager;
    User user;
    AwesomeValidation validator;
    Call<Group> call;

    private OnFragmentInteractionListener mListener;

    public AddGroupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddGroupFragment.
     */
    public static AddGroupFragment newInstance() {
        AddGroupFragment fragment = new AddGroupFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    public void setupRules() {
        validator.addValidation(getActivity(), R.id.add_group_name, RegexTemplate.NOT_EMPTY, R.string.error_field_required);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_group, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addGroupll = getActivity().findViewById(R.id.add_group_ll);
        groupImageView = getActivity().findViewById(R.id.add_group_image);
        tilName = getActivity().findViewById(R.id.add_group_name);
        tilDescription = getActivity().findViewById(R.id.add_group_description);
        checkIsPublic = getActivity().findViewById(R.id.is_public);
        validator = new AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT);
        setupRules();

        Button submitBtn = getActivity().findViewById(R.id.add_group_submit);
        groupImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upload();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilName.setError(null);
                validator.clear();
                if (validator.validate()) {

                    String groupName = tilName.getEditText().getText().toString();
                    String groupDescription = tilDescription.getEditText().getText().toString();
                    boolean isPublic = checkIsPublic.isChecked();
                    call = service.postGroup(user.getId(), groupName, groupDescription, isPublic);

                    call.enqueue(new Callback<Group>() {
                        @Override
                        public void onResponse(Call<Group> call, Response<Group> response) {
                            if (response.isSuccessful()) {
                                Log.w(TAG, "onResponse: success");
                                if (selectedFilePath != null && !selectedFilePath.isEmpty()) {
                                    long id = response.body().getId();
                                    File file = new File(selectedFilePath);
                                    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                                    MultipartBody.Part body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
                                    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

                                    retrofit2.Call<Group> req = service.postGroupImage(user.getId(), id, body, name);
                                    req.enqueue(new Callback<Group>() {
                                        @Override
                                        public void onResponse(Call<Group> call, Response<Group> response) {
                                            if (response.isSuccessful()) {
                                                Log.w(TAG, "file upload: success");
                                                Toast.makeText(getContext(), "Groupe créé", Toast.LENGTH_LONG).show();
                                            } else {
                                                ApiError apiError_ = Utils.convertErrors(response.errorBody());
                                                Log.w(TAG, "file upload: no success " + apiError_.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Group> call, Throwable t) {
                                            Log.w(TAG, "file upload: failed");
                                        }
                                    });
                                } else {
                                    Toast.makeText(getContext(), "Groupe créé", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                ApiError apiError = Utils.convertErrors(response.errorBody());
                                Log.w(TAG, "onResponse: no success " + apiError.getMessage());
                                Toast.makeText(getContext(), "An error occured", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Group> call, Throwable t) {
                            Log.e(TAG, "onFailure group creation: " + t.getMessage());
                            Toast.makeText(getContext(), "An error occured", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

        });

    }

    void upload() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            showFileChooser();

        } else if (getActivity().checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            showFileChooser();
        } else {
            if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                Snackbar.make(addGroupll, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
                            }
                        });
            } else {
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    void showFileChooser() {
        Log.w(TAG, "showFileChooser");
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_FILE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    return;
                }
                Uri selectedFileUri = data.getData();
                selectedFilePath = FilePath.getPath(getActivity(), selectedFileUri);
                Log.w(TAG, "Selected file path: " + selectedFilePath);

                File file = new File(selectedFilePath);
                groupImageView.setPadding(0, 0, 0, 0);
                groupImageView.setBackground(null);
                GlideApp.with(getActivity())
                        .load(file)
                        .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                        .transition(withCrossFade())
                        .transform(new CircleTransform(getActivity()))
                        .into(groupImageView);

            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
