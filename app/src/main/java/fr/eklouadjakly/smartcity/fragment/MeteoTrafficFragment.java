package fr.eklouadjakly.smartcity.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Main;
import fr.eklouadjakly.smartcity.model.OpenWeather;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.model.Weather;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MeteoTrafficFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MeteoTrafficFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeteoTrafficFragment extends Fragment {

    private static final String TAG = "MeteoTrafficFragment";
    private static final int REQUEST_ACCESS_LOCATION = 0;

    private LinearLayout meteoll;
    private TextView meteoInfo;
    TokenManager tokenManager;
    User user = null;

    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;

    Typeface weatherFont;

    ApiService service;
    Call<OpenWeather> call;

    private OnFragmentInteractionListener mListener;

    public MeteoTrafficFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MeteoTrafficFragment.
     */

    public static MeteoTrafficFragment newInstance() {
        MeteoTrafficFragment fragment = new MeteoTrafficFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = RetrofitBuilder.createServiceForOpenWeatherMap(ApiService.class);
        tokenManager = TokenManager.getInstance(getActivity().getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meteo_traffic, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        meteoll = getActivity().findViewById(R.id.meteoll);
        meteoInfo = getActivity().findViewById(R.id.meteo_info);
        if (user.getLocation().getCountry() == null || user.getLocation().getCity() == null) {
            meteoInfo.setText(getString(R.string.no_address_found));
        }
        weatherFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/weathericons-regular-webfont.ttf");

        cityField = getActivity().findViewById(R.id.city_field);
        updatedField = getActivity().findViewById(R.id.updated_field);
        detailsField = getActivity().findViewById(R.id.details_field);
        currentTemperatureField = getActivity().findViewById(R.id.current_temperature_field);
        humidity_field = getActivity().findViewById(R.id.humidity_field);
        pressure_field = getActivity().findViewById(R.id.pressure_field);
        weatherIcon = getActivity().findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);

        //On récupère la météo
        if (user.getLocation().getCountry() != null && user.getLocation().getCity() != null) {
            String q = user.getLocation().getCity() + "," + user.getLocation().getCountry();
            call = service.weather(q, "metric", RetrofitBuilder.getOpenWeatherMapApi());
            call.enqueue(new Callback<OpenWeather>() {
                @Override
                public void onResponse(Call<OpenWeather> call, Response<OpenWeather> response) {
                    if (response.isSuccessful()) {
                        OpenWeather openWeather = response.body();
                        if (openWeather != null) {
                            Weather details = openWeather.getWeather().get(0);
                            Main main = openWeather.getMain();
                            DateFormat df = DateFormat.getDateTimeInstance();
                            String city = openWeather.getName().toUpperCase() + ", " + openWeather.getSys().getCountry();
                            String description = details.getDescription().toUpperCase();
                            String temperature = String.format("%.1f", main.getTemp()) + "°";
                            String humidity = main.getHumidity() + "%";
                            String pressure = main.getPressure() + " hPa";
                            Log.w(TAG, "onResponse: timestamp " + openWeather.getDt());
//                            String updatedOn = df.format(new Date(openWeather.getDt() * 1000));
                            long timestamp = Long.parseLong(String.valueOf(openWeather.getDt())) * 1000L;
                            String updatedOn = getDate(timestamp);
                            String iconText = setWeatherIcon(details.getId(),
                                    openWeather.getSys().getSunrise() * 1000,
                                    openWeather.getSys().getSunrise() * 1000);

                            cityField.setText(city);
                            updatedField.setText(updatedOn);
                            detailsField.setText(description);
                            currentTemperatureField.setText(temperature);

                            humidity_field.setText(getString(R.string.humidity) + ": "+ humidity);
                            pressure_field.setText(getString(R.string.pressure) + ": " + pressure);
                            weatherIcon.setText(Html.fromHtml(iconText));
                        }
                    } else {
                        Log.w(TAG, "onResponse: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<OpenWeather> call, Throwable t) {
                    Log.w(TAG, "onFailure: " + t.getMessage());
                }
            });
        }

    }

    private String getDate(long timeStamp){

        try{
            DateFormat sdf = SimpleDateFormat.getDateTimeInstance();
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
    public static String setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = "&#xf00d;";
            } else {
                icon = "&#xf02e;";
            }
        } else {
            switch (id) {
                case 2:
                    icon = "&#xf01e;";
                    break;
                case 3:
                    icon = "&#xf01c;";
                    break;
                case 7:
                    icon = "&#xf014;";
                    break;
                case 8:
                    icon = "&#xf013;";
                    break;
                case 6:
                    icon = "&#xf01b;";
                    break;
                case 5:
                    icon = "&#xf019;";
                    break;
            }
        }
        return icon;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }
}
