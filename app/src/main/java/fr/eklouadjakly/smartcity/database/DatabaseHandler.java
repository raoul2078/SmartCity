package fr.eklouadjakly.smartcity.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.model.User;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "smartcity";

    public static final String TABLE_USER = "user";

    public static final String USER_ID = "id";
    public static final String USER_FIRSTNAME = "firstname";
    public static final String USER_LASTNAME = "lastname";
    public static final String USER_EMAIL = "email";
    public static final String USER_DATE_OF_BIRTH = "date_of_birth";
    public static final String USER_CITY = "city";
    public static final String USER_COUNTRY = "country";
    public static final String USER_AUTH_WITH_GOOGLE = "auth_with_google";
    public static final String USER_PICTURE = "picture";
    public static final String USER_ROLES = "roles";

    public static final String CREATE_TABLE_USER =
            "CREATE TABLE " + TABLE_USER + "(" +
                    USER_ID + " INTEGER PRIMARY KEY, " +
                    USER_FIRSTNAME + " VARCHAR(255) NOT NULL, " +
                    USER_LASTNAME + " VARCHAR(255) NULL, " +
                    USER_EMAIL + " VARCHAR(255) NOT NULL, " +
                    USER_DATE_OF_BIRTH + " TEXT NULL, " +
                    USER_CITY + " VARCHAR(255) NULL, " +
                    USER_COUNTRY + " VARCHAR(255) NULL, " +
                    USER_PICTURE + " VARCHAR(255) NULL, " +
                    USER_AUTH_WITH_GOOGLE + " TINYINT(3) NULL, " +
                    USER_ROLES + " VARCHAR(255) NOT NULL)";

    public static final String TABLE_USER_DROP = "DROP TABLE IF EXISTS " + TABLE_USER;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_USER_DROP);
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    // -----------------Gestion des données de l'utilisateur-------//
    public void addUser(User u) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_ID, u.getId());
        values.put(USER_FIRSTNAME, u.getFirstname());
        values.put(USER_LASTNAME, u.getLastname());
        values.put(USER_EMAIL, u.getEmail());
        values.put(USER_DATE_OF_BIRTH, u.getDateOfBirth());
        values.put(USER_CITY, u.getLocation().getCity());
        values.put(USER_COUNTRY, u.getLocation().getCountry());
        values.put(USER_PICTURE, u.getPicture());
        values.put(USER_AUTH_WITH_GOOGLE, u.getAuthWithGoogle());
        values.put(USER_ROLES, u.getRoles().get(0));

        db.insert(TABLE_USER, null, values);
    }

    public int updateUser(User u) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_FIRSTNAME, u.getFirstname());
        values.put(USER_LASTNAME, u.getLastname());
        values.put(USER_EMAIL, u.getEmail());
        values.put(USER_DATE_OF_BIRTH, u.getDateOfBirth());
        values.put(USER_CITY, u.getLocation().getCity());
        values.put(USER_COUNTRY, u.getLocation().getCountry());
        values.put(USER_AUTH_WITH_GOOGLE, u.getAuthWithGoogle());
        values.put(USER_ROLES, u.getRoles().get(0));
        return db.update(TABLE_USER, values,
                USER_ID + "=?", new String[]{String.valueOf(u.getId())});

    }

    public User getUser(long userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_USER + " WHERE " + USER_ID + " = " + userId;
        Log.e(TAG, "getUser: " + selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        User u = null;
        if (c != null) {
            u = new User();
            c.moveToFirst();

            u.setId(c.getInt(c.getColumnIndex(USER_ID)));
            u.setFirstname(c.getString(c.getColumnIndex(USER_FIRSTNAME)));
            u.setLastname(c.getString(c.getColumnIndex(USER_LASTNAME)));
            u.setEmail(c.getString(c.getColumnIndex(USER_EMAIL)));
            u.setDateOfBirth(c.getString(c.getColumnIndex(USER_DATE_OF_BIRTH)));
            int i = c.getInt(c.getColumnIndex(USER_AUTH_WITH_GOOGLE));
            boolean auth = (i == 1);
            u.setAuthWithGoogle(auth);
            u.getLocation().setCity(c.getString(c.getColumnIndex(USER_CITY)));
            u.getLocation().setCountry(c.getString(c.getColumnIndex(USER_COUNTRY)));
            u.setPicture(c.getString(c.getColumnIndex(USER_PICTURE)));
            List<String> roles = new ArrayList<>();
            roles.add(c.getString(c.getColumnIndex(USER_ROLES)));
            u.setRoles(roles);
        }
        return u;
    }

    public void deleteToDo(long userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, USER_ID + " = ?",
                new String[] { String.valueOf(userId) });
    }
}
