package fr.eklouadjakly.smartcity.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.TradingAdapter;
//import fr.eklouadjakly.smartcity.fragment.SocialFragment;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.SortTradings;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.support.v7.widget.DividerItemDecoration.*;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class ShopsSubscribingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ShopsSubscribingActivit";
    private static final int REQUEST_ACCESS_LOCATION = 0;
    private static final int FILTER_RESULT = 1;

    Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TokenManager tokenManager;
    String profileUrl;
    TextView usernameView;
    NavigationView navigationView;
    ConstraintLayout shoplayout;
    SwipeRefreshLayout swipeRefreshLayout;
    private TradingAdapter mAdapter;
    private RecyclerView mRecyclerView;
    ApiService service;
    Call<List<Trading>> call;

    private List<Trading> tradings = new ArrayList<>();
    private List<Trading> nearestTrading = new ArrayList<>();

    FusedLocationProviderClient mFusedLocationClient;

    private double lat, lng;

    String selectedInterest = null;
    int chosenTradings = FilterShopsActivity.TRADINGS_EVERYWHERE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_subscribing);

        shoplayout = findViewById(R.id.shoplayout);
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }
        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_shops));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Location permission
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //on récupère les infos de l'utilisateur
        User user;
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();
        if (user != null) {

            //Firebase token manager
            service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

            loadRecyclerViewData();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.listshow);
        mRecyclerView.setHasFixedSize(true);
        ItemDecoration itemDecoration = new DividerItemDecoration(this, VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
    }

    private void setTradingsAdapter(List<Trading> list) {
        mAdapter = new TradingAdapter(this, list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shops_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.shops_search:
                startActivity(new Intent(this, ShopsSearchActivity.class));
                return true;
            case R.id.shops_filter:
                startActivityForResult(new Intent(this, FilterShopsActivity.class), FILTER_RESULT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Demande d'autorisation
     */
    public void requestPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }
        if (checkSelfPermission(ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION) || shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            Snackbar.make(shoplayout, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
                        }
                    });
        } else {
            requestPermissions(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
        }
    }

    public void getNearestTradings() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        Toast.makeText(ShopsSubscribingActivity.this, "Veuillez activer votre GPS", Toast.LENGTH_LONG).show();
                        return;
                    }
                    lat = location.getLatitude();
                    lng = location.getLongitude();

                    Log.w(TAG, "initlocation onSuccess: " + lat + ", " + lng);

                    LatLng latLng = new LatLng(lat, lng);
                    Collections.sort(mAdapter.getData(), new SortTradings(latLng));
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_LOCATION) {
            if (grantResults.length > 0) {
                boolean finePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean coarsePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (finePermission && coarsePermission) {
                    getNearestTradings();
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        if(selectedInterest != null && selectedInterest.equals("all")){
            selectedInterest = null;
        }
        call = service.getTradings(selectedInterest);
        call.enqueue(new Callback<List<Trading>>() {

            @Override
            public void onResponse(Call<List<Trading>> call, Response<List<Trading>> response) {
                if (response.isSuccessful()) {
                    Log.w(TAG, "tradings response: " + response.body().size());
                    if (response.body() != null) {
                        if (response.body().isEmpty()) {
                            Toast.makeText(ShopsSubscribingActivity.this, "No results", Toast.LENGTH_LONG).show();
                        } else {
                            tradings.clear();
                            tradings.addAll(response.body());
                            setTradingsAdapter(tradings);
                            if(chosenTradings == FilterShopsActivity.TRADINGS_NEAR) {
                                getNearestTradings();
                            }
                        }
                    } else {
                        Toast.makeText(ShopsSubscribingActivity.this, "Error", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "trading status " + response.code());
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Trading>> call, Throwable t) {
                Toast.makeText(ShopsSubscribingActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, "trading failure " + t.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILTER_RESULT) {
            if(resultCode == RESULT_OK) {
                selectedInterest = data.getStringExtra(FilterShopsActivity.INTEREST_CHOSEN);
                chosenTradings = data.getIntExtra(FilterShopsActivity.TRADINGS_CHOSEN, FilterShopsActivity.TRADINGS_NEAR);
                loadRecyclerViewData();
            }
        }
    }
}
