package fr.eklouadjakly.smartcity.activity;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.GroupsAdapter;
import fr.eklouadjakly.smartcity.adapter.TradingAdapter;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopsSearchActivity extends AppCompatActivity {

    private static final String TAG = "ShopsSearchActivity";

    Toolbar toolbar;
    RecyclerView recyclerView;
    TradingAdapter myAdapter;

    ApiService service;

    Call<List<Trading>> call;
    TokenManager tokenManager;
    User user;

    private List<Trading> tradings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_search);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_shops));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = findViewById(R.id.tradingsrv);

        tokenManager = TokenManager.getInstance(this.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shops_search_options, menu);

        // Associate searchable configuration with the SearchView
        MenuItem searchMenuItem = menu.findItem(R.id.shops_search);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) searchMenuItem.getActionView();

        searchView.setIconified(false);
        searchMenuItem.expandActionView();
        searchView.requestFocus();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return false;
            }

            //On gère l'appui sur le bouton retour: on veut retourner sur la derniere activité
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                finish();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                call = service.findTradingsByName(query);
                call.enqueue(new Callback<List<Trading>>() {
                    @Override
                    public void onResponse(Call<List<Trading>> call, Response<List<Trading>> response) {
                        if(response.isSuccessful()) {
                            if(response.body() != null) {
                                if(response.body().isEmpty()) {
                                    Toast.makeText(ShopsSearchActivity.this, "No results", Toast.LENGTH_LONG).show();
                                } else {
                                    tradings.clear();
                                    tradings.addAll(response.body());
                                    setTradingsAdapter(tradings);
                                }
                            }
                        } else {
                            ApiError apiError = Utils.convertErrors(response.errorBody());
                            Toast.makeText(ShopsSearchActivity.this, apiError.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Trading>> call, Throwable t) {
                        Toast.makeText(ShopsSearchActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                        Log.w(TAG, "onFailure: "+t.getMessage() );
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setTradingsAdapter(List<Trading> list) {
        myAdapter = new TradingAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (call != null) {
            call.cancel();
            call = null;
        }
    }
}
