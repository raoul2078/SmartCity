package fr.eklouadjakly.smartcity.activity;

import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.ViewPagerAdapter;
import fr.eklouadjakly.smartcity.fragment.AlarmFragment;
import fr.eklouadjakly.smartcity.fragment.EventsFragment;
import fr.eklouadjakly.smartcity.fragment.HeadLinesFragment;
import fr.eklouadjakly.smartcity.fragment.MeteoTrafficFragment;

public class NewsActivity extends AppCompatActivity {

    Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_news));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tabLayout = findViewById(R.id.news_tabs);
        viewPager = findViewById(R.id.news_viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new MeteoTrafficFragment(), getString(R.string.meteo_traffic));
        adapter.addFragment(new HeadLinesFragment(), getString(R.string.headlines));
        adapter.addFragment(new EventsFragment(), getString(R.string.events));
        adapter.addFragment(new AlarmFragment(), getString(R.string.alarm));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        //Google location
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public FusedLocationProviderClient getmFusedLocationClient() {
        return mFusedLocationClient;
    }
}
