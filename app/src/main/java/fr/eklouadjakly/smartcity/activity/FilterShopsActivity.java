package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.HashMap;

import fr.eklouadjakly.smartcity.R;

public class FilterShopsActivity extends AppCompatActivity {

    private static final String TAG = "FilterShopsActivity";
    public static int TRADINGS_EVERYWHERE = 1;
    public static int TRADINGS_NEAR = 2;
    public static String TRADINGS_CHOSEN = "TRADING_CHOSEN";
    public static String INTEREST_CHOSEN = "INTEREST_CHOSEN";

    String interests[];
    HashMap<String, String> hashMap;
    RadioGroup radioGroup;
    RadioButton selectedButton;
    Button saveButton;
    Toolbar toolbar;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_shops);

        interests = new String[]{
                getString(R.string.all),
                getString(R.string.business),
                getString(R.string.health),
                getString(R.string.science),
                getString(R.string.technology),
                getString(R.string.sport),
                getString(R.string.cars),
                getString(R.string.cooking),
                getString(R.string.meubles),
                getString(R.string.music),
                getString(R.string.mode),
        };

        hashMap = new HashMap<>();
        hashMap.put(getString(R.string.all), "all");
        hashMap.put(getString(R.string.business), "business");
        hashMap.put(getString(R.string.health), "health");
        hashMap.put(getString(R.string.science), "science");
        hashMap.put(getString(R.string.technology), "technology");
        hashMap.put(getString(R.string.sport), "sport");
        hashMap.put(getString(R.string.cars), "cars");
        hashMap.put(getString(R.string.cooking), "cooking");
        hashMap.put(getString(R.string.meubles), "meubles");
        hashMap.put(getString(R.string.music), "music");

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.filter_shops));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        spinner = findViewById(R.id.interests_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, interests);

// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        radioGroup = findViewById(R.id.radiogroupTradings);

        saveButton = findViewById(R.id.submitFilter);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectId = radioGroup.getCheckedRadioButtonId();
                selectedButton = findViewById(selectId);
                int selectedTrading = selectedButton.getText().equals(getString(R.string.near)) ? TRADINGS_NEAR : TRADINGS_EVERYWHERE;
                String selectedInterest = hashMap.get((String) spinner.getSelectedItem());

                Log.w(TAG, "save filtershops: " + selectedButton.getText() + " spinner : " + selectedInterest);
                Intent intent = new Intent();
                intent.putExtra(TRADINGS_CHOSEN, selectedTrading);
                intent.putExtra(INTEREST_CHOSEN, selectedInterest);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
