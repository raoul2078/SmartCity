package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    DrawerLayout profileDrawer;
    ImageView profilePictureView;
    TextView usernameView;
    TextView usercityView;
    TokenManager tokenManager;
    User user;

    String profileUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        profileDrawer = findViewById(R.id.profileDrawer);
        profilePictureView = findViewById(R.id.profilePicture);
        usernameView = findViewById(R.id.username);
        usercityView = findViewById(R.id.usercity);
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();

        //Setup toolbar
        toolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // toggle = new ActionBarDrawerToggle(this, profileDrawer, R.string.open, R.string.close);
        // profileDrawer.addDrawerListener(toggle);
        // toggle.syncState();

        if (user != null) {

            //Charger l'image
            profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();
            GlideApp.with(this)
                    .load(profileUrl)
                    .signature(new ObjectKey(tokenManager.getUUID()))
                    .apply(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                    .transition(withCrossFade())
                    .transform(new CircleTransform(this))
                    //  .diskCacheStrategy(DiskCacheStrategy.NONE)

                    .into(profilePictureView);

            String username = user.getFirstname() + " " + user.getLastname();
            usernameView.setText(username);
            if (user.getLocation().getCity() != null) {
                String city = user.getLocation().getCity() + ", " + user.getLocation().getCountry();
                usercityView.setText(city);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.edit_profile:
                startActivity(new Intent(this, EditProfileActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_options, menu);
        return true;
    }


    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        user = tokenManager.getUser();
        profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();
        GlideApp.with(this)
                .load(profileUrl)
                .signature(new ObjectKey(tokenManager.getUUID()))
                .apply(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                .transition(withCrossFade())
                .transform(new CircleTransform(this))
                //  .diskCacheStrategy(DiskCacheStrategy.NONE)

                .into(profilePictureView);
    }
}
