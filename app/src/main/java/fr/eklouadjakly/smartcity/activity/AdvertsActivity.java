package fr.eklouadjakly.smartcity.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.AdvertsAdapter;
import fr.eklouadjakly.smartcity.adapter.NotificationsAdapter;
import fr.eklouadjakly.smartcity.model.Advert;
import fr.eklouadjakly.smartcity.model.Notification;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdvertsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "AdvertsActivity";
    Toolbar toolbar;
    RecyclerView recyclerView;
    AdvertsAdapter myAdapter;

    ApiService service;
    Call<List<Advert>> call;
    TokenManager tokenManager;
    User user;

    SwipeRefreshLayout swipeRefreshLayout;
    private List<Advert> adverts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adverts);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_adverts));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }
        recyclerView = findViewById(R.id.advertsrv);

        tokenManager = TokenManager.getInstance(this.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);

        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        call = service.getAdverts(user.getId());

        call.enqueue(new Callback<List<Advert>>() {
            @Override
            public void onResponse(Call<List<Advert>> call, Response<List<Advert>> response) {
                if (response.isSuccessful()) {
                    Log.w(TAG, "onResponse: success "+response.body());
                    if (!response.body().isEmpty()) {
                        adverts.clear();
                        adverts.addAll(response.body());
                        Log.w(TAG, "advert: "+adverts.size() );
                        setAdvertsAdapter(adverts);
                    } else {
                        Toast.makeText(AdvertsActivity.this, R.string.no_ads_available, Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (response.code() == 401) {
                        tokenManager.deleteToken();
                        tokenManager.removeUser();
                        tokenManager.removeUUID();

                        finish();
                        startActivity(new Intent(AdvertsActivity.this, LoginActivity.class));
                    }
                    ApiError apiError = Utils.convertErrors(response.errorBody());
                    Log.w(TAG, "onResponse error : " + apiError.getMessage());
                    Toast.makeText(AdvertsActivity.this, apiError.getMessage(), Toast.LENGTH_LONG).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Advert>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(AdvertsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    private void setAdvertsAdapter(List<Advert> list) {
        myAdapter = new AdvertsAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

}
