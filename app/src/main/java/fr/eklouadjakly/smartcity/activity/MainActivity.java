package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.fragment.AdvertsFragment;
import fr.eklouadjakly.smartcity.fragment.HomeFragment;
import fr.eklouadjakly.smartcity.fragment.InterestsFragment;
import fr.eklouadjakly.smartcity.fragment.SettingsFragment;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = "MainActivity";
    ImageView profilePictureView;
    TextView usernameView;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    TokenManager tokenManager;
    User user;

    String profileUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_home));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // on récupère la headerview
        navigationView = (NavigationView) findViewById(R.id.navigation);
        Log.w(TAG, "onCreate: " + navigationView);
        View headerView = navigationView.inflateHeaderView(R.layout.header);
        profilePictureView = headerView.findViewById(R.id.profilePicture);
        usernameView = headerView.findViewById(R.id.username);

        //Affichage du profil
        TextView profileText = headerView.findViewById(R.id.profile);
        profileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                drawerLayout.closeDrawers();
            }
        });
        //  ButterKnife.bind(this);
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));


        setupDrawerContent(navigationView);
        //on récupère les infos de l'utilisateur

        user = tokenManager.getUser();

        if (user != null) {
            Log.w(TAG, "onCreate: " + RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture());
            //Charger l'image
            profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();
            GlideApp.with(this)
                    .load(profileUrl)
                    .signature(new ObjectKey(tokenManager.getUUID()))
                    .transition(withCrossFade())
                    .transform(new CircleTransform(this))
                    //    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(profilePictureView);

            if (tokenManager.getPlaceId() == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent, HomeFragment.newInstance(profileUrl)).commit();
                getSupportActionBar().setTitle(getString(R.string.menu_home));
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent, HomeFragment.newInstance()).commit();
                getSupportActionBar().setTitle(getString(R.string.menu_home));
            }
            String username = user.getFirstname() + " " + user.getLastname();
            Log.i(TAG, "onCreate: " + username);
            usernameView.setText(username);

            //Firebase token manager
            ApiService service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
            Log.w(TAG, "tcmtoken " + user.getFcmToken());
            String regId = tokenManager.getRegId();
            if (regId != null) {
                if (!regId.equals(user.getFcmToken())) {
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    user.setFcmToken(tokenManager.getRegId());
                    Log.w(TAG, "tcmtoken after update " + user.getFcmToken());
                    Call<User> call = service.patchUser(user.getId(), user);
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if (response.isSuccessful()) {
                                Log.w(TAG, "onResponse success: " + response.body());
                                tokenManager.saveUser(response.body());
                            } else {
                                ApiError apiError = Utils.convertErrors(response.errorBody());
                                Log.w(TAG, "onResponse failure: " + apiError.getMessage() + " " + response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Log.w(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                }
            }
        }

    }


    /**
     * Navigation par le menu latéral
     *
     * @param menuItem Element menu
     */
    public void selectItemDrawer(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.interests:
                fragmentClass = InterestsFragment.class;
                break;
            case R.id.home:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.news:
                startActivity(new Intent(this, NewsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.shops:
                startActivity(new Intent(this, ShopsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.socials:
                startActivity(new Intent(this, SocialsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.adverts:
                startActivity(new Intent(this, AdvertsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.settings:
                fragmentClass = SettingsFragment.class;
                break;
            case R.id.logout:
                logout();
                return;
            default:
                fragmentClass = HomeFragment.class;
        }
        try {
            if (fragmentClass == HomeFragment.class) {
                fragment = HomeFragment.newInstance(profileUrl);
            } else {
                fragment = (Fragment) fragmentClass.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flcontent, fragment).commit();
        menuItem.setChecked(true);
        getSupportActionBar().setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();
    }

    /**
     * Navigation
     *
     * @param id Identifiant card
     */

    public void selectItemDrawerByCard(int id) {
        Fragment fragment = null;
        Class fragmentClass;
        String title;
        switch (id) {
            case R.id.newscard:
                startActivity(new Intent(this, NewsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.shopscard:
                startActivity(new Intent(this, ShopsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.socialscard:
                startActivity(new Intent(this, SocialsActivity.class));
                drawerLayout.closeDrawers();
                return;
            case R.id.advertscard:
                startActivity(new Intent(this, AdvertsActivity.class));
                drawerLayout.closeDrawers();
                return;
            default:
                fragmentClass = HomeFragment.class;
                title = getString(R.string.menu_home);

        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flcontent, fragment).commit();
        getSupportActionBar().setTitle(title);
        drawerLayout.closeDrawers();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                selectItemDrawer(item);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return toggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void logout() {
        ApiService service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
        Call<ResponseBody> call = service.logout(tokenManager.getToken().getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    tokenManager.deleteToken();
                    tokenManager.removeUser();
                    tokenManager.removeUUID();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.get(MainActivity.this).clearDiskCache();
                        }
                    }).start();
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        Glide.get(getApplicationContext()).clearMemory();
        finish();
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();
        user = tokenManager.getUser();
        if (user != null) {
            profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();
            GlideApp.with(this)
                    .load(profileUrl)
                    .signature(new ObjectKey(tokenManager.getUUID()))
                    .transition(withCrossFade())
                    .transform(new CircleTransform(this))
                    //    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(profilePictureView);
            getSupportFragmentManager().beginTransaction().replace(R.id.flcontent, HomeFragment.newInstance(profileUrl)).commit();
        }
    }
}
