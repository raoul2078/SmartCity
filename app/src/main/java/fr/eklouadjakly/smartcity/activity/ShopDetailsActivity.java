package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.OfferAdapter;
import fr.eklouadjakly.smartcity.adapter.TradingAdapter;
import fr.eklouadjakly.smartcity.model.Offer;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.utils.TokenManager;

public class ShopDetailsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;
    TokenManager tokenManager;
    private OfferAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private static final String TAG = "ShopDetailsActivity";
    SwipeRefreshLayout swipeRefreshLayout;
    Trading trading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);

        String tradingString = getIntent().getStringExtra(TradingAdapter.TRADING_DATA);
        Log.w(TAG, "onCreate: tradingstring "+tradingString );
        trading = new Gson().fromJson(tradingString, Trading.class);

        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefreshLayout.setProgressViewOffset(false, 0, 500);
        }

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(trading.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.listshowoffer);
        mRecyclerView.setHasFixedSize(true);

        loadRecyclerViewData();

    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    public void loadRecyclerViewData() {
        swipeRefreshLayout.setRefreshing(true);
        setOfferAdapter(trading.getOffers());
        swipeRefreshLayout.setRefreshing(false);
    }

    private void setOfferAdapter(List<Offer> list) {
        mAdapter = new OfferAdapter(this, list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
