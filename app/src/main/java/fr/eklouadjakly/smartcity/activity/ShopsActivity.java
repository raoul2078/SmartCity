package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.fragment.AdvertsFragment;
import fr.eklouadjakly.smartcity.fragment.HomeFragment;
import fr.eklouadjakly.smartcity.fragment.InterestsFragment;
import fr.eklouadjakly.smartcity.fragment.SettingsFragment;
import fr.eklouadjakly.smartcity.fragment.ShopFragment;
//import fr.eklouadjakly.smartcity.fragment.SocialFragment;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ShopsActivity extends AppCompatActivity {

    Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TokenManager tokenManager;
    private static final String TAG = "ShopsActivity";
    String profileUrl;
    TextView usernameView;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_shops));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //on récupère les infos de l'utilisateur
        User user;
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", MODE_PRIVATE));
        user = tokenManager.getUser();

        if (user != null) {
            Log.w(TAG, "onCreate: " + RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture());
            //Charger l'image
            profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + user.getPicture();

            String username = user.getFirstname() + " " + user.getLastname();
            Log.i(TAG, "onCreate: " + username);
            getSupportFragmentManager().beginTransaction().replace(R.id.flcontentShop, ShopFragment.newInstance(profileUrl)).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onOptionsCardSelected(int id) {
        Fragment fragment = null;
        Class fragmentClass;
        String title;
        switch (id) {
            case R.id.shopcard2:
                startActivity(new Intent(this, ShopsSubscribingActivity.class));
                return;
            case R.id.shopcard1:
                startActivity(new Intent(this, ShopsOfferActivity.class));
                return;
            default:
                fragmentClass = HomeFragment.class;
                title = getString(R.string.menu_home);

        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flcontentShop, fragment).commit();
        getSupportActionBar().setTitle(title);
    }


}
