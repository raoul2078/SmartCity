package fr.eklouadjakly.smartcity.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.ViewPagerAdapter;
import fr.eklouadjakly.smartcity.fragment.AddGroupFragment;
import fr.eklouadjakly.smartcity.fragment.MyGroupsFragment;

public class SocialsActivity extends AppCompatActivity {

    Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socials);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.menu_socials));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tabLayout = findViewById(R.id.socials_tabs);
        viewPager = findViewById(R.id.socials_viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new MyGroupsFragment(), getString(R.string.my_groups));
      //  adapter.addFragment(new DiscoverGroupsFragment(), getString(R.string.discover_groups));
        adapter.addFragment(new AddGroupFragment(), getString(R.string.add_group));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.socials_options, menu);
       // menu.getItem(1).setActionView(R.layout.menu_dot);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.socials_search:
                startActivity(new Intent(this, SocialsSearchActivity.class));
                return true;
            case R.id.socials_notifications:
                startActivity(new Intent(this, NotificationsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
