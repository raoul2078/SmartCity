package fr.eklouadjakly.smartcity.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.GroupsAdapter;
import fr.eklouadjakly.smartcity.adapter.PostsAdapter;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.Post;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class GroupActivity extends AppCompatActivity {

    private static final String TAG = "GroupActivity";
    private static final int ADD_POST = 1;
    private static final int EDIT_GROUP = 2;
    public static final String GROUP_EDIT = "fr.eklouadjakly.smartcity.activity.GROUP_EDIT";
    public static final String USERS_DATA = "fr.eklouadjakly.smartcity.activity.USERS_DATA";

    TextView nameView, descriptionView, nbMembers, nbPosts;
    ImageView imageView, settingsView;
    RecyclerView recyclerView;

    AppBarLayout appbar;
    Toolbar toolbar;
    Group group;

    PostsAdapter myAdapter;

    TokenManager tokenManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();

        final String groupAsString = getIntent().getStringExtra(GroupsAdapter.GROUP_DATA);
        group = new Gson().fromJson(groupAsString, Group.class);


        //Setup toolbar
        appbar = findViewById(R.id.appbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsingtoolbar);
        //collapsing.setTitle(radioGroup.getName());
        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(group.getName());
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });

        nameView = findViewById(R.id.group_name);
        descriptionView = findViewById(R.id.group_description);
        nbMembers = findViewById(R.id.members_number);
        nbPosts = findViewById(R.id.posts_number);
        imageView = findViewById(R.id.group_image);
        settingsView = findViewById(R.id.group_settings);

        nameView.setText(group.getName());
        descriptionView.setText(group.getDescription());
        nbMembers.setText(Integer.toString(group.getMembers().size()));
        nbPosts.setText(Integer.toString(group.getPosts().size()));
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + group.getImage();

        GlideApp.with(this)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(this))
                .into(imageView);

        recyclerView = findViewById(R.id.postsrv);
        setPostsAdapter(group.getPosts());

        //Group settings activity
        if (!user.getId().equals(group.getAdmin().getId())) {
            settingsView.setVisibility(View.GONE);
        } else {
            settingsView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GroupActivity.this, EditGroupActivity.class);
                    intent.putExtra(GROUP_EDIT, new Gson().toJson(group));
                    startActivityForResult(intent, EDIT_GROUP);
                }
            });
        }
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupActivity.this, AddPostActivity.class);
                intent.putExtra(GroupsAdapter.GROUP_DATA, new Gson().toJson(group));
                startActivityForResult(intent, ADD_POST);
            }
        });

        // On dirige vers la liste des membres
        nbMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w(TAG, "onClick: nbMembers");
                Intent intent = new Intent(GroupActivity.this, MembersActivity.class);
                intent.putExtra(USERS_DATA, new Gson().toJson(group));
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Initialisation de l'adapter
     *
     * @param list Posts list
     */
    private void setPostsAdapter(List<Post> list) {
        myAdapter = new PostsAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_POST) {
                String newGroupAsString = data.getStringExtra(GroupsAdapter.GROUP_DATA);
                group = new Gson().fromJson(newGroupAsString, Group.class);
                myAdapter.setData(group.getPosts());
                myAdapter.notifyDataSetChanged();
                nameView.setText(group.getName());
                descriptionView.setText(group.getDescription());
                nbMembers.setText(Integer.toString(group.getMembers().size()));
                nbPosts.setText(Integer.toString(group.getPosts().size()));
            } else if (requestCode == EDIT_GROUP) {
                String newGroupAsString = data.getStringExtra(GroupActivity.GROUP_EDIT);
                group = new Gson().fromJson(newGroupAsString, Group.class);
                myAdapter.setData(group.getPosts());
                myAdapter.notifyDataSetChanged();
                nameView.setText(group.getName());
                descriptionView.setText(group.getDescription());
                nbMembers.setText(Integer.toString(group.getMembers().size()));
                nbPosts.setText(Integer.toString(group.getPosts().size()));
                String newProfileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + group.getImage();


                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(GroupActivity.this).clearDiskCache();
                    }
                }).start();

                Glide.get(getApplicationContext()).clearMemory();*/
                GlideApp.with(this)
                        .load(newProfileUrl)
                        .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                        .skipMemoryCache(true)
                        .transition(withCrossFade())
                        .transform(new CircleTransform(this))
                        .into(imageView);
            }
        }
    }

    public boolean isMember(User u, Group g) {
        if (g.getAdmin().getId().equals(u.getId())) {
            return true;
        }
        for (User user : g.getMembers()) {
            if (user.getId().equals(u.getId())) {
                return true;
            }
        }
        return false;
    }
}
