package fr.eklouadjakly.smartcity.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.adapter.UsersAdapter;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.utils.TokenManager;

public class MembersActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;

    TokenManager tokenManager;
    User user;
    List<User> members = new ArrayList<>();

    UsersAdapter myAdapter;
    Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);
        recyclerView = findViewById(R.id.usersrv);

        tokenManager = TokenManager.getInstance(getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        final String membersAsString = getIntent().getStringExtra(GroupActivity.USERS_DATA);
        group = new Gson().fromJson(membersAsString, Group.class);
        members = group.getMembers();
        setUsersAdapter(members);

        //Setup toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(group.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUsersAdapter(List<User> list) {
        myAdapter = new UsersAdapter(this, list, group);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
    }
}
