package fr.eklouadjakly.smartcity.utils;

import android.content.SharedPreferences;
import android.location.Address;

import com.google.gson.Gson;

import java.util.ArrayList;

import fr.eklouadjakly.smartcity.model.AccessToken;
import fr.eklouadjakly.smartcity.model.Interest;
import fr.eklouadjakly.smartcity.model.Location;
import fr.eklouadjakly.smartcity.model.User;

public class TokenManager {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private static TokenManager INSTANCE = null;

    private TokenManager(SharedPreferences prefs) {
        this.prefs = prefs;
        this.editor = prefs.edit();
    }

    public static synchronized TokenManager getInstance(SharedPreferences prefs) {
        if (INSTANCE == null) {
            INSTANCE = new TokenManager(prefs);
        }

        return INSTANCE;
    }

    public void saveUser(User u) {
        String s = u.getDateOfBirth();
        if (s != null && !s.isEmpty()) {
            if (s.length() > 10) {
                String d = s.substring(0, 10);
                u.setDateOfBirth(d);
            }
        }
        String userJSONString = new Gson().toJson(u);
        editor.putString("USER", userJSONString).commit();
        incUUID();
    }

    public User getUser() {
        String userJSONString = prefs.getString("USER", null);
        if (userJSONString == null) {
            return null;
        }
        User user = new Gson().fromJson(userJSONString, User.class);
        if (user.getLocation() == null) {
            user.setLocation(new Location());
        }
        if (user.getInterests() == null) {
            user.setInterests(new ArrayList<Interest>());
        }
        return user;
    }

    public void removeUser() {
        editor.remove("USER").commit();
    }

    public void saveAddress(Address a) {
        String addressJSONString = new Gson().toJson(a);
        editor.putString("ADDRESS", addressJSONString).commit();
    }

    public Address getAddress() {
        String addressJSONString = prefs.getString("ADDRESS", null);
        if (addressJSONString == null) {
            return null;
        }
        return new Gson().fromJson(addressJSONString, Address.class);
    }

    public void removeAddress() {
        editor.remove("ADDRESS").commit();
    }

    //    public void saveProfile(String profile) {editor.putString("Profile", profile).commit();}
//    public void deleteProfile(String profile) {editor.remove("Profile").commit();}
//    public void removeProfile
    public void saveToken(AccessToken token) {
        editor.putString("ACCESS_TOKEN", token.getValue()).commit();
        editor.putInt("TOKEN_ID", token.getId()).commit();
        editor.putString("CREATED_AT", token.getCreatedAt()).commit();
    }

    public void deleteToken() {
        editor.remove("ACCESS_TOKEN").commit();
        editor.remove("TOKEN_ID").commit();
        editor.remove("CREATED_AT").commit();
    }

    public AccessToken getToken() {
        AccessToken token = new AccessToken();
        token.setValue(prefs.getString("ACCESS_TOKEN", null));
        token.setCreatedAt(prefs.getString("CREATED_AT", null));
        token.setId(prefs.getInt("TOKEN_ID", 0));
        return token;
    }


    public long getUUID() {
        return prefs.getLong("UUID", 0);
    }

    private long incUUID() {
        long i = getUUID();
        editor.putLong("UUID", i + 1).commit();
        return i + 1;
    }

    public void removeUUID() {
        editor.remove("UUID").commit();
    }

    public String getRegId() {
        return prefs.getString("REG_ID", null);
    }

    public void saveRegId(String reg) {
        editor.putString("REG_ID", reg).commit();
    }

    public void removeRegId() {
        editor.remove("REG_ID").commit();
    }

    public void alarmOn() {
        editor.putBoolean("ALARM", true).commit();
    }

    public void alarmOff() {
        editor.putBoolean("ALARM", false).commit();
    }

    public boolean isAlarmSet() {
        return prefs.getBoolean("ALARM", false);
    }

    public void setAlarm(String hour) {
        editor.putString("ALARM_HOUR", hour).commit();
    }

    public String getAlarm() {
        return prefs.getString("ALARM_HOUR", "");
    }

    public void savePlaceId(String id) {
        editor.putString("PLACE_ID", id).commit();
    }

    public void removePlaceId() {
        editor.remove("PLACE_ID").commit();
    }

    public String getPlaceId() {
        return prefs.getString("PLACE_ID", null);
    }
}
