package fr.eklouadjakly.smartcity.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import fr.eklouadjakly.smartcity.BuildConfig;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    private static final String LOCAL_URL = "http://localhost:8000/";
    private static final String NEWS_URL = "https://newsapi.org/v2/everything/";
    private static final String NEWS_API_KEY = "a3f15aa6ac2f4ae281904e8876e069e9";

    private static final String OPEN_WEATHER_MAP_URL =
            "http://api.openweathermap.org/data/2.5/weather/";



    private static final String OPEN_WEATHER_MAP_API = "f8c3244537a0c957e850e1ff01d138ce";

    public static String getRemoteUrl() {
        return REMOTE_URL;
    }

    private static final String REMOTE_URL = "http://176.138.112.121/SmartCity-server/public/";
    private static final String BASE_URL = REMOTE_URL + "index.php/api/";

    private final static OkHttpClient client = buildClient();
    private final static Retrofit retrofit = buildRetrofit(client);
    private final static Retrofit newsRetrofit = buildNewsRetrofit(client);
    private final static Retrofit openWeatherRetrofit = buildOpenWeatherRetrofit(client);

    private static OkHttpClient buildClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();

                        Request.Builder builder = request.newBuilder()
                                .addHeader("Accept", "application/json")
                                .addHeader("Connection", "close");

                        request = builder.build();

                        return chain.proceed(request);
                    }
                })
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                ;

        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        return builder.build();
    }

    private static Retrofit buildRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    private static Retrofit buildNewsRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(NEWS_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    private static Retrofit buildOpenWeatherRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(OPEN_WEATHER_MAP_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    public static <T> T createService(Class<T> service) {
        return retrofit.create(service);
    }

    public static <T> T createServiceWithAuth(Class<T> service, final TokenManager tokenManager) {

        OkHttpClient newClient = client.newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder builder = request.newBuilder();
                if (tokenManager.getToken().getValue() != null) {
                    builder.addHeader("X-Auth-Token", tokenManager.getToken().getValue());
                }

                request = builder.build();
                return chain.proceed(request);
            }
        }).authenticator(CustomAuthenticator.getInstance(tokenManager)).build();

        Retrofit newRetrofit = retrofit.newBuilder().client(newClient).build();
        return newRetrofit.create(service);
    }

    public static  <T> T createServiceForNewsApi(Class<T> service,  TokenManager tokenManager) {
        OkHttpClient newClient = client.newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder builder = request.newBuilder();
                builder.addHeader("X-Api-Key", NEWS_API_KEY);

                request = builder.build();
                return chain.proceed(request);
            }
        }).authenticator(CustomAuthenticator.getInstance(tokenManager)).build();

        Retrofit newRetrofit = newsRetrofit.newBuilder().client(newClient).build();
        return newRetrofit.create(service);
    }

    public static  <T> T createServiceForOpenWeatherMap(Class<T> service) {
        OkHttpClient newClient = client.newBuilder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder builder = request.newBuilder();
                builder.addHeader("x-api-key", OPEN_WEATHER_MAP_API);

                request = builder.build();
                return chain.proceed(request);
            }
        }).build();

        Retrofit newRetrofit = openWeatherRetrofit.newBuilder().client(newClient).build();
        return newRetrofit.create(service);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getOpenWeatherMapApi() {
        return OPEN_WEATHER_MAP_API;
    }

    private static Gson getGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }
}
