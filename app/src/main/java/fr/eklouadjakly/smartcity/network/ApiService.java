package fr.eklouadjakly.smartcity.network;

import java.util.List;

import fr.eklouadjakly.smartcity.model.AccessToken;
import fr.eklouadjakly.smartcity.model.Advert;
import fr.eklouadjakly.smartcity.model.AuthResponse;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.InterestRequest;
import fr.eklouadjakly.smartcity.model.NewsApi;
import fr.eklouadjakly.smartcity.model.Notification;
import fr.eklouadjakly.smartcity.model.Offer;
import fr.eklouadjakly.smartcity.model.OpenWeather;
import fr.eklouadjakly.smartcity.model.Post;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.model.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    @GET("users")
    Call<List<User>> getUsers();

    @GET("tradings")
    Call<List<Trading>> getTradings(@Query("interest") String interest);


    @GET("offers")
    Call<List<Offer>> getOffers();

    @POST("users")
    @FormUrlEncoded
    Call<AuthResponse> signup(@Field("firstname") String firstname, @Field("lastname") String lastname, @Field("email") String email, @Field("plainPassword") String plainPassword);

    @PATCH("users/{id}")
    Call<User> patchUser(@Path("id") long id, @Body User user);

    @PATCH("users/{id}/interests")
        //@FormUrlEncoded
    Call<User> updateUserInterests(@Path("id") long id, @Body InterestRequest interests);

    @POST("auth-tokens")
    @FormUrlEncoded
    Call<AuthResponse> login(@Field("login") String login, @Field("password") String password);

    @POST("google-sign-in")
    @FormUrlEncoded
    Call<AuthResponse> googleSignin(@Field("idToken") String idToken);

    @POST("refresh")
    @FormUrlEncoded
    Call<AccessToken> refresh(@Field("token") String token, @Field("userId") long userId);

    @GET(".")
    Call<NewsApi> news(@Query("language") String language, @Query("q") String q, @Query("from") String from, @Query("pageSize") int pageSize, @Query("page") int page);

    @GET(".")
    Call<OpenWeather> weather(@Query("q") String q, @Query("units") String units, @Query("APPID") String id);

    @Multipart
    @POST("users/{id}/profile-picture")
    Call<User> postImage(@Path("id") long id, @Part MultipartBody.Part image, @Part("name") RequestBody name);

    @FormUrlEncoded
    @POST("posts/{id}/comments")
    Call<Post> comment(@Path("id") long id, @Field("content") String content);

    @FormUrlEncoded
    @POST("groups/{id}/posts")
    Call<Group> addPost(@Path("id") long id, @Field("title") String title, @Field("content") String content);


    @DELETE("auth-tokens/{id}")
    Call<ResponseBody> logout(@Path("id") long id);


    @FormUrlEncoded
    @POST("users/{id}/groups")
    Call<Group> postGroup(@Path("id") long userId, @Field("name") String name, @Field("description") String description, @Field("isPublic") boolean isPublic);

    @Multipart
    @POST("users/{id}/groups/{group_id}/image")
    Call<Group> postGroupImage(@Path("id") long id, @Path("group_id") long group_id, @Part MultipartBody.Part image, @Part("name") RequestBody name);

    @GET("users/{id}/groups")
    Call<List<Group>> getMyGroups(@Path("id") long id);

    @POST("groups/{id}/ask-to-join")
    Call<ResponseBody> askToJoinGroup(@Path("id") long id);

    @GET("groups/{name}")
    Call<List<Group>> findGroupsByName(@Path("name") String name);

    @FormUrlEncoded
    @POST("groups/{id}/members")
    Call<Group> joinGroup(@Path("id") long id, @Field("member") long member, @Field("notification") long notification);

    @DELETE("groups/{id}/members/{member}")
    Call<Group> removeMember(@Path("id") long id, @Path("member") long member);

    @FormUrlEncoded
    @PATCH("groups/{id}")
    Call<Group> patchGroup(@Path("id") long groupId, @Field("name") String name, @Field("description") String description, @Field("isPublic") boolean isPublic);

    @GET("notifications")
    Call<List<Notification>> getNotifications();

    @DELETE("notifications/{id}")
    Call<ResponseBody> deleteNotification(@Path("id") long id);

    @GET("users/{id}/adverts")
    Call<List<Advert>> getAdverts(@Path("id") long id);

    @POST("users/{id}/tradings")
    @FormUrlEncoded
    Call<User> addUserTrading(@Path("id") long userId, @Field("tradings") List<Integer> tradings);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "users/{id}/tradings", hasBody = true)
    Call<User> deleteUserTrading(@Path("id") long userId, @Field("tradings") List<Integer> tradings);

    @GET("tradings/search/{name}")
    Call<List<Trading>> findTradingsByName(@Path("name") String name);

    @GET("offers/{name}")
    Call<List<Offer>> findOffersByName(@Path("name") String name);
}
