package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Advert;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class AdvertsAdapter extends RecyclerView.Adapter<AdvertsAdapter.MyViewHolder> {
    private static final String TAG = "AdvertsAdapter";
    private Context context;
    private List<Advert> data;
    TokenManager tokenManager;
    User user;
    ApiService service;
    Call<List<Advert>> call;

    public AdvertsAdapter(Context context, List<Advert> data) {
        this.context = context;
        this.data = data;
    }

    public List<Advert> getData() {
        return data;
    }

    public void setData(List<Advert> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public AdvertsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view;
        view = inflater.inflate(R.layout.advert_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdvertsAdapter.MyViewHolder holder, int position) {
        holder.title.setText(data.get(position).getTitle());
        holder.description.setText(data.get(position).getDescription());
        String contentUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getContent();

        if (data.get(position).getType().equals("IMAGE")) {
            GlideApp.with(context)
                    .load(contentUrl)
                    .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                    .override(600, 400)
                    .into(holder.img);
        } else {
            holder.img.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.VISIBLE);
            holder.videoView.setVideoPath(contentUrl);
            MediaController mediaController = new
                    MediaController(context);
            mediaController.setAnchorView(holder.videoView);
            holder.videoView.setMediaController(mediaController);

            holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.i(TAG, "Duration = " +
                            holder.videoView.getDuration());
                }
            });
             holder.videoView.start();
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, description;
        ImageView img;
        VideoView videoView;
        LinearLayout content;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.advertstitle);
            description = itemView.findViewById(R.id.advertsdescription);
            img = itemView.findViewById(R.id.advertsimg);
            videoView = itemView.findViewById(R.id.advertsvideo);
            content = itemView.findViewById(R.id.advertscontent);
        }
    }
}
