package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.google.gson.Gson;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.activity.EditProfileActivity;
import fr.eklouadjakly.smartcity.activity.GroupActivity;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiError;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import fr.eklouadjakly.smartcity.utils.Utils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.MyViewHolder> {
    private Context context;
    private List<Group> data;
    public static String GROUP_DATA = "fr.eklouadjakly.smartcity.adapter.GROUP_DATA";
    TokenManager tokenManager;
    User user;
    ApiService service;
    Call<ResponseBody> call;

    public GroupsAdapter(Context context, List<Group> list) {
        this.context = context;
        this.data = list;
        tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    public List<Group> getData() {
        return data;
    }

    public void setData(List<Group> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.group_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.viewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String groupAsString = new Gson().toJson(data.get(viewHolder.getAdapterPosition()));
                Intent intent = new Intent(context, GroupActivity.class);
                //Si le groupe est public
                if(data.get(viewHolder.getAdapterPosition()).getPublic() || isMember(user, data.get(viewHolder.getAdapterPosition()))) {
                    intent.putExtra(GROUP_DATA, groupAsString);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, R.string.group_private, Toast.LENGTH_LONG).show();
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getImage();
        holder.name.setText(data.get(position).getName());
        holder.description.setText(data.get(position).getDescription());

        GlideApp.with(context)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(context))
                .into(holder.groupImg);
        if(isMember(user, data.get(position))) {
            holder.joinGroup.setVisibility(View.GONE);
        }

        holder.joinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call = service.askToJoinGroup(data.get(position).getId());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()) {
                            Toast.makeText(context, "Votre demande a été envoyée à l'administrateur", Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, description;
        ImageView groupImg;
        Button joinGroup;
        LinearLayout viewContent;


        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.group_name);
            description = itemView.findViewById(R.id.group_description);
            groupImg = itemView.findViewById(R.id.group_image);
            joinGroup = itemView.findViewById(R.id.join_group);
            viewContent = itemView.findViewById(R.id.groupcontent);
        }
    }

    public boolean isMember(User u, Group g) {
        if (g.getAdmin().getId().equals(u.getId())) {
            return true;
        }
        for (User user : g.getMembers()) {
            if (user.getId().equals(u.getId())) {
                return true;
            }
        }
        return false;
    }
}
