package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.Notification;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    private Context context;
    private List<Notification> data;
    TokenManager tokenManager;
    User user;
    ApiService service;
    Call<Group> call;

    public NotificationsAdapter(Context context, List<Notification> data) {
        this.context = context;
        this.data = data;
        tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    public List<Notification> getData() {
        return data;
    }

    public void setData(List<Notification> data) {
        this.data = data;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.notification_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getSender().getPicture();
        String name = data.get(position).getSender().getFirstname() + " "+data.get(position).getSender().getLastname();
        holder.name.setText(name);
        String msg = data.get(position).getMessage();
        if(msg.equals("JOIN GROUP")) {
            holder.message.setText(context.getString(R.string.join_group,  data.get(position).getGroup().getName()));
        } else if(msg.equals("NEW POST")) {
            holder.message.setText(context.getString(R.string.new_post, data.get(position).getGroup().getName()));
            holder.accept.setVisibility(View.GONE);
            holder.refuse.setText(R.string.delete_notification);
        }
        GlideApp.with(context)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(context))
                .into(holder.senderImg);

        // Accepter la demande de rajout au groupe
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call = service.joinGroup(data.get(position).getGroup().getId(), data.get(position).getSender().getId(), data.get(position).getId());
                call.enqueue(new Callback<Group>() {
                    @Override
                    public void onResponse(Call<Group> call, Response<Group> response) {
                        if (response.isSuccessful()) {
                            data.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, data.size());
                        } else {
                            Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Group> call, Throwable t) {
                        Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        // Refuser
        holder.refuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> deleteCall = service.deleteNotification(data.get(position).getId());
                deleteCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            data.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, data.size());
                        } else {
                            Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, message;
        ImageView senderImg;
        Button accept, refuse;
        LinearLayout viewContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.notification_sender);
            message = itemView.findViewById(R.id.notification_msg);
            senderImg = itemView.findViewById(R.id.notification_sender_image);
            accept = itemView.findViewById(R.id.accept);
            refuse = itemView.findViewById(R.id.refuse);
            viewContent = itemView.findViewById(R.id.notification_content);
        }
    }
}
