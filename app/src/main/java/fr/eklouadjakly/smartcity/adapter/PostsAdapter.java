package fr.eklouadjakly.smartcity.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.activity.PostActivity;
import fr.eklouadjakly.smartcity.model.Post;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.MyViewHolder>{
    private Context context;
    private List<Post> data;
    public static String POST_DATA = "fr.eklouadjakly.smartcity.adapter.POST_DATA";

    public PostsAdapter(Context context, List<Post> list) {
        this.context = context;
        this.data = list;
    }


    public List<Post> getData() {
        return data;
    }

    public void setData(List<Post> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.post_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.viewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String postAsString = new Gson().toJson(data.get(viewHolder.getAdapterPosition()));
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra(POST_DATA, postAsString);
                context.startActivity(intent);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getUser().getPicture();
        String name = data.get(position).getUser().getFirstname() + " "+data.get(position).getUser().getLastname();
        DateFormat df = DateFormat.getDateTimeInstance();
        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

        try {
            Date d = new SimpleDateFormat(DATE_FORMAT_PATTERN).parse(data.get(position).getCreatedAt().substring(0,19));
            String date = df.format(d);
            holder.createdAt.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.username.setText(name);
        holder.postTitle.setText(data.get(position).getTitle());
        holder.postContent.setText(data.get(position).getContent());
        holder.nbComments.setText(Integer.toString(data.get(position).getComments().size()));
        GlideApp.with(context)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(context))
                .into(holder.profileView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView username, createdAt, postTitle, postContent, nbComments;
        ImageView profileView;
        LinearLayout viewContent;
        public MyViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.username);
            createdAt = itemView.findViewById(R.id.createdAt);
            postTitle = itemView.findViewById(R.id.post_title);
            postContent = itemView.findViewById(R.id.post_content);
            nbComments = itemView.findViewById(R.id.comment_number);
            profileView = itemView.findViewById(R.id.user_profile);
            viewContent =  itemView.findViewById(R.id.post_ll);
        }
    }
}
