package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.activity.ShopDetailsActivity;
import fr.eklouadjakly.smartcity.model.Trading;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Faniry on 29/04/2018.
 */

public class TradingAdapter extends RecyclerView.Adapter<TradingAdapter.MyViewHolder> {

    private static final String TAG = "TradingAdapter";
    public static String TRADING_DATA = "TRADING_DATA";
    private Context context;
    private List<Trading> data;
    private TokenManager tokenManager;
    User user;
    ApiService service;
    Call<Trading> call;

    public TradingAdapter(Context context, List<Trading> data) {
        this.context = context;
        this.data = data;

        tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    public boolean isSubscriber(Trading trading) {
        if (trading.getUsers() != null) {
            for (User u : trading.getUsers()) {
                if (u.getId().equals(user.getId())) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @NonNull
    @Override
    public TradingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.activity_shop_subscribing_item, parent, false);
        final TradingAdapter.MyViewHolder viewHolder = new TradingAdapter.MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TradingAdapter.MyViewHolder holder, final int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl();
        String buffer;

        //on rempli MyViewHolder
        buffer = data.get(position).getName();
        Log.w(TAG, "onBindViewHolder tradingadapter Trading:  " + buffer);
        holder.name.setText(buffer);
        buffer = data.get(position).getCompleteAddress();
        Log.w(TAG, "onBindViewHolder tradingadapter Location: " + buffer);
        holder.location.setText(buffer);

        holder.viewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShopDetailsActivity.class);
                String tradingString = (new Gson()).toJson(data.get(position));
                intent.putExtra(TRADING_DATA, tradingString);
                Log.w(TAG, "trading data "+tradingString);
                context.startActivity(intent);
            }
        });
        final List<Integer> tradingList = new ArrayList<>();
        tradingList.add(data.get(position).getId());
        if (isSubscriber(data.get(position))) {
            holder.sub.setText(context.getResources().getText(R.string.unsubscribe));
            holder.sub.setTextColor(context.getResources().getColor(R.color.white));
            holder.sub.setBackground(context.getResources().getDrawable(R.drawable.button_green));

            holder.sub.setOnClickListener(new DeleteTradingListener(holder, tradingList));
        } else {
            holder.sub.setOnClickListener(new AddTradingListener(holder, tradingList));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Trading> getData() {
        return data;
    }

    public void setData(List<Trading> data) {
        this.data = data;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView location;
        Button sub;
        LinearLayout viewContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.trading_name);
            location = itemView.findViewById(R.id.trading_location);
            sub = itemView.findViewById(R.id.tradingsub);
            viewContent = itemView.findViewById(R.id.tradingcontent);
        }
    }

    /**
     * Listener pour la suppression des abonnements à un commerce
     */
    private class DeleteTradingListener implements View.OnClickListener {
        MyViewHolder holder;
        List<Integer> tradingList;

        public DeleteTradingListener(MyViewHolder holder, List<Integer> tradingList) {
            this.holder = holder;
            this.tradingList = tradingList;
        }

        @Override
        public void onClick(View v) {
            Call<User> deleteCall = service.deleteUserTrading(user.getId(), tradingList);
            deleteCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, "Désabonnement effectué", Toast.LENGTH_LONG).show();
                        holder.sub.setText(context.getResources().getText(R.string.subscribe));
                        holder.sub.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        holder.sub.setBackground(context.getResources().getDrawable(R.drawable.button_outline));

                        // on change de listener
                        holder.sub.setOnClickListener(new AddTradingListener(holder, tradingList));
                    } else {
                        Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onFailure: delete user trading subscription " + response.code());

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure: delete user trading subscription " + t.getMessage());
                }
            });
        }
    }

    /**
     * Listener pour l'ajout d'un abo à un commerce
     */
    private class AddTradingListener implements View.OnClickListener {
        MyViewHolder holder;
        List<Integer> tradingList;

        public AddTradingListener(MyViewHolder holder, List<Integer> tradingList) {
            this.holder = holder;
            this.tradingList = tradingList;
        }

        @Override
        public void onClick(View v) {
            Call<User> addCall = service.addUserTrading(user.getId(), tradingList);
            addCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(context, "Abonnement effectué", Toast.LENGTH_LONG).show();
                        holder.sub.setText(context.getResources().getText(R.string.unsubscribe));
                        holder.sub.setTextColor(context.getResources().getColor(R.color.white));
                        holder.sub.setBackground(context.getResources().getDrawable(R.drawable.button_green));

                        // on change de listener
                        // on change de listener
                        holder.sub.setOnClickListener(new DeleteTradingListener(holder, tradingList));
                    } else {
                        Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onFailure: add user trading subscription " + response.code());

                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure: delete user trading subscription " + t.getMessage());
                }
            });
        }
    }
}
