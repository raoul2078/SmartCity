package fr.eklouadjakly.smartcity.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Event;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private Context context;
    private List<Event> data;

    public EventsAdapter(Context context, List<Event> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public EventsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view;
        view = inflater.inflate(R.layout.event_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.eventContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long eventID = data.get(viewHolder.getAdapterPosition()).getId();
                Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
                Intent intent = new Intent(Intent.ACTION_EDIT)
                        .setData(uri)
                        .putExtra(CalendarContract.Events.TITLE, "My New Title");
                context.startActivity(intent);

            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(data.get(position).getTitle());
        holder.description.setText(data.get(position).getDescription());
        holder.location.setText(data.get(position).getLocation());
        holder.begin.setText(data.get(position).getBegin());
      //  holder.end.setText(data.get(position).getEnd());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, description, location, begin, end;
        LinearLayout eventContent;
        public MyViewHolder(View itemView) {
            super(itemView);
            eventContent = itemView.findViewById(R.id.eventContent);
            title = itemView.findViewById(R.id.eventTitle);
            description = itemView.findViewById(R.id.eventDescription);
            location = itemView.findViewById(R.id.eventLocation);
            begin = itemView.findViewById(R.id.eventBegin);
          //  end = itemView.findViewById(R.id.eventEnd);
        }
    }
}
