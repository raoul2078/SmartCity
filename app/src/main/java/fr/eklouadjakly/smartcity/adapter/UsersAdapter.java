package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Group;
import fr.eklouadjakly.smartcity.model.User;
import fr.eklouadjakly.smartcity.network.ApiService;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;
import fr.eklouadjakly.smartcity.utils.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder> {
    private Context context;
    private List<User> data;
    TokenManager tokenManager;
    User user;
    ApiService service;
    Call<Group> call;
    Group group;

    public UsersAdapter(Context context, List<User> data, Group group) {
        this.context = context;
        this.data = data;
        this.group = group;

        tokenManager = TokenManager.getInstance(context.getSharedPreferences("prefs", Context.MODE_PRIVATE));
        user = tokenManager.getUser();
        service = RetrofitBuilder.createServiceWithAuth(ApiService.class, tokenManager);
    }

    @NonNull
    @Override
    public UsersAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.user_item, parent, false);
        final UsersAdapter.MyViewHolder viewHolder = new UsersAdapter.MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersAdapter.MyViewHolder holder, final int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getPicture();
        String username = data.get(position).getFirstname() + " " + data.get(position).getLastname();
        holder.name.setText(username);

        GlideApp.with(context)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(context))
                .into(holder.userImg);

        // Retirer un utilisateur d'un groupe
        if(!user.getId().equals(group.getAdmin().getId())){
            holder.removeUser.setVisibility(View.GONE);
        } else {
            holder.removeUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Call<Group> call = service.removeMember(group.getId(), data.get(position).getId());
                    call.enqueue(new Callback<Group>() {
                        @Override
                        public void onResponse(Call<Group> call, Response<Group> response) {
                            if (response.isSuccessful()) {
                                data.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, data.size());
                            } else {
                                Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Group> call, Throwable t) {
                            Toast.makeText(context, "An error occured", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<User> getData() {
        return data;
    }

    public void setData(List<User> data) {
        this.data = data;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView userImg;
        Button removeUser;
        LinearLayout viewContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.user_name);
            userImg = itemView.findViewById(R.id.user_image);
            removeUser = itemView.findViewById(R.id.remove_user);
            viewContent = itemView.findViewById(R.id.usercontent);
        }
    }
}
