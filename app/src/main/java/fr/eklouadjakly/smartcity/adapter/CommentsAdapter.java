package fr.eklouadjakly.smartcity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Comment;
import fr.eklouadjakly.smartcity.network.RetrofitBuilder;
import fr.eklouadjakly.smartcity.utils.CircleTransform;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {
    private Context context;
    private List<Comment> data;

    public CommentsAdapter(Context context, List<Comment> data) {
        this.context = context;
        this.data = data;
    }

    public List<Comment> getData() {
        return data;
    }

    public void setData(List<Comment> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.comment_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String profileUrl = RetrofitBuilder.getRemoteUrl() + "img/" + data.get(position).getUser().getPicture();
        String name = data.get(position).getUser().getFirstname() + " "+data.get(position).getUser().getLastname();
        DateFormat df = DateFormat.getDateTimeInstance();
        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

        try {
            Date d = new SimpleDateFormat(DATE_FORMAT_PATTERN).parse(data.get(position).getCreatedAt().substring(0,19));
            String date = df.format(d);
            holder.createdAt.setText(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.username.setText(name);
        holder.commentContent.setText(data.get(position).getContent());

        GlideApp.with(context)
                .load(profileUrl)
                .apply(new RequestOptions().format(DecodeFormat.PREFER_RGB_565))
                .transition(withCrossFade())
                .transform(new CircleTransform(context))
                .into(holder.profileView);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView username, createdAt, commentContent;
        ImageView profileView;
        LinearLayout viewContent;
        public MyViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            createdAt = itemView.findViewById(R.id.createdAt);
            commentContent = itemView.findViewById(R.id.comment_content);
            profileView = itemView.findViewById(R.id.user_profile);
            viewContent =  itemView.findViewById(R.id.comment_ll);
        }
    }
}
