package fr.eklouadjakly.smartcity.adapter;


import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import fr.eklouadjakly.smartcity.GlideApp;
import fr.eklouadjakly.smartcity.R;
import fr.eklouadjakly.smartcity.model.Article;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    private Context context;
    private List<Article> data;

    public NewsAdapter(Context context, List<Article> list) {
        this.context = context;
        data = list;

    }

    @NonNull
    @Override
    public NewsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.news_item, parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.viewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = data.get(viewHolder.getAdapterPosition()).getUrl();
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.addDefaultShareMenuItem();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(context, Uri.parse(url));
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.MyViewHolder holder, int position) {
        holder.title.setText(data.get(position).getTitle());
        String str = data.get(position).getPublishedAt();

        //format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(str);
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            holder.date.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.author.setText(data.get(position).getAuthor());

        GlideApp.with(context).load(data.get(position).getUrlToImage()).override(600, 400).into(holder.newsImg);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, date, author;
        ImageView newsImg;
        LinearLayout viewContent;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.newstitle);
            date = itemView.findViewById(R.id.newsdate);
            author = itemView.findViewById(R.id.newsauthor);
            newsImg = itemView.findViewById(R.id.newsimg);
            viewContent = itemView.findViewById(R.id.newscontent);
        }

    }

}
