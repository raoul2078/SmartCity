package fr.eklouadjakly.smartcity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("authWithGoogle")
    @Expose
    private Boolean authWithGoogle;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("fcmToken")
    @Expose
    private String fcmToken = null;
    @SerializedName("interests")
    @Expose(serialize = false, deserialize = false)
    private List<Interest> interests = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getAuthWithGoogle() {
        return authWithGoogle;
    }

    public void setAuthWithGoogle(Boolean authWithGoogle) {
        this.authWithGoogle = authWithGoogle;
    }

    public List<String> getRoles() {
        return roles;
    }

    public List<Interest> getInterests() {
        return interests;
    }

    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
