package fr.eklouadjakly.smartcity.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Faniry on 29/04/2018.
 */

public class Offer {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("product")
    @Expose
    private String product;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("trading")
    @Expose
    private Trading trading;

    @SerializedName("category")
    @Expose
    private Interest category;

    @SerializedName("image")
    @Expose
    private String image;

    public Offer(Integer id, String product, String brand, float price, Trading trading, Interest category, String image) {
        this.id = id;
        this.product = product;
        this.brand = brand;
        this.price = price;
        this.trading = trading;
        this.category = category;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Trading getTrading() {
        return trading;
    }

    public void setTrading(Trading trading) {
        this.trading = trading;
    }

    public Interest getCategory() {
        return category;
    }

    public void setCategory(Interest category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
