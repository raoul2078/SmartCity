package fr.eklouadjakly.smartcity.model;


import java.util.Date;

public class Event {
    private Long id;
    private String title;
    private String description;
    private String location;
    private String begin;
    private String end;

    public Event(Long id, String title, String description, String location, String begin, String end) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.location = location;
        this.begin = begin;
        this.end = end;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
